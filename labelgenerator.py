import qrcode
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.lib.utils import ImageReader
from io import BytesIO
from PIL import Image

# Function for creating the QR code and adding it to the PDF
def create_qr_code(asn: int):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=10,
        border=0,
    )
    qr.add_data(f"ASN{asn:03d}")
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    buffer = BytesIO()
    img.save(buffer, format='PNG')
    return ImageReader(Image.open(buffer))

# Creating the PDF
c = canvas.Canvas("output.pdf", pagesize=A4)
pagesize_x = c._pagesize[0]
pagesize_y = c._pagesize[1]
x_border = 12*mm # Margin: Left & Right
y_border = 12.5*mm # Margin: Top & Bottom
#y_margin = 5*mm # Spacing between rows
#x_margin = 10*mm # Spacing between columns

# HP LaseJet 1008s: label_height = 10.03*mm
label_height = 10.02*mm
label_width = 28*mm
qr_size = 8*mm

#label_skip = 7*5
#firstASN = 22
#label_count = 1

label_skip = (8*7)+0
firstASN = 48
label_count = 0     # 0 = no limit

# Calculation of the number of columns and rows
#label_width = qr_size + 2*mm + c.stringWidth(f"{0:06d}", "Helvetica", qr_size/mm-3) + x_margin
#columns = int((pagesize_x - 2*x_border - 4*mm) / label_width)
columns = 7

#lines = int((pagesize_y - 2*y_border) / (qr_size + y_margin))
#lines = int((pagesize_y - y_border) / label_height)
lines = 27
print(f"columns: {columns:02d}, lines: {lines:02d}")

def calcCoordinates(counter: int):
    column = (counter - 1) % columns
    line = (counter - 1) // columns
    print(f"{counter:03d} column: {column:02d}, line: {line:02d}")
    return (x_border + column * label_width, 
        y_border + label_height + (label_height * line))

def getXPos(x: float):
    #   Origin + QR + Spacing (1/6 QR code) 
    return x + qr_size + qr_size/6

i = label_skip ## Generating the QR codes and adding them to the PDF
if label_count == 0: label_count = (columns * lines) - label_skip 
for asn in range(firstASN, firstASN + label_count):
    i += 1
    x, y = calcCoordinates(i)
    qr = create_qr_code(asn)

    # add QR code to PDF
    c.drawImage(qr, x, pagesize_y-y, width=qr_size, height=qr_size)

    # add Label to PDF
    c.setFont("Helvetica", qr_size/mm+2)
    c.drawString(getXPos(x), (pagesize_y-y)+(qr_size/2)+(qr_size/16),"ASN")
    c.setFont("Helvetica",  qr_size/mm)
    c.drawString(getXPos(x), (pagesize_y-y)+(qr_size/6), f"{asn:06d}")

c.save()
print("The PDF file has been successfully created: output.pdf")